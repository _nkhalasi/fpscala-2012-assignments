object app_state {
  def sqrt(x: Double) = {
    def abs(x: Double) = if (x < 0) -x else x

    def isGoodEnough(guess: Double) =
      abs(guess * guess - x) / x < 0.001

    def improve(guess: Double) =
      (guess + x / guess) / 2

    def sqrtIter(guess: Double): Double =
      if (isGoodEnough(guess)) guess
      else sqrtIter(improve(guess))
    
    sqrtIter(1.0)
  }                                               //> sqrt: (x: Double)Double

	def monitored(f: Double => Double) = {
		var counter = 0
		def mf(cmd:Any) = {
			cmd match {
				case "how-many-calls?" => counter
				case "reset" => counter=0; counter
				case _ => counter = counter + 1; f(_)
			}
		}
		mf _
	}                                         //> monitored: (f: Double => Double)Any => Any
	
	val monitored_sqrt = monitored(sqrt)      //> monitored_sqrt  : Any => Any = <function1>
	monitored_sqrt(25)                        //> res0: Any = <function1>
	monitored_sqrt(16)                        //> res1: Any = <function1>
  monitored_sqrt("how-many-calls?")               //> res2: Any = 2
  monitored_sqrt("reset")                         //> res3: Any = 0
  monitored_sqrt(25)                              //> res4: Any = <function1>
  monitored_sqrt(25)                              //> res5: Any = <function1>
  monitored_sqrt(16)                              //> res6: Any = <function1>
  monitored_sqrt(9)                               //> res7: Any = <function1>
  monitored_sqrt("how-many-calls?")               //> res8: Any = 4
  monitored_sqrt("reset")                         //> res9: Any = 0
  
}