import week3._

object intsets {
  val t1 = new Empty incl 3                       //> t1  : week3.IntSet = {.3.}
  val t2 = t1 incl 4                              //> t2  : week3.IntSet = {.3{.4.}}
  val t3 = new Empty incl 7                       //> t3  : week3.IntSet = {.7.}
  val t4 = new Empty incl 5                       //> t4  : week3.IntSet = {.5.}
  val t5 = t2 union t3                            //> t5  : week3.IntSet = {{{.3.}4.}7.}
  val t6 = t5 union t4                            //> t6  : week3.IntSet = {{{.3.}4.}5{.7.}}
  val t7 = new Empty incl 6                       //> t7  : week3.IntSet = {.6.}
  t2 union t7 union t3 union t4 incl 10 incl 8 incl 1
                                                  //> res0: week3.IntSet = {{{{.1.}3.}4.}5{.6{.7{{.8.}10.}}}}
}