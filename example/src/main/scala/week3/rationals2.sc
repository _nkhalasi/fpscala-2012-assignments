object rationals2 {
  val x = new Rational2(1,3)                      //> x  : Rational2 = 1/3
	val y = new Rational2(5,7)                //> y  : Rational2 = 5/7
	val z = new Rational2(3,2)                //> z  : Rational2 = 3/2
	x.add(y)                                  //> res0: Rational2 = 22/21
	x.sub(y)                                  //> res1: Rational2 = 8/-21
	x.sub(y).sub(z)                           //> res2: Rational2 = -79/42
	
	y.add(y)                                  //> res3: Rational2 = 10/7
	x.less(y)                                 //> res4: Boolean = true
	x.max(y)                                  //> res5: Rational2 = 5/7

	
	new Rational2(2)                          //> res6: Rational2 = 2/1
}

class Rational2(x:Int, y:Int) {
	require(y != 0, "denominator must be nonzero")
	
	def this(x: Int) = this(x, 1)
	
	private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
	
	def numer = x
	def denom = y
	
	def less(that: Rational2) = numer * that.denom  < that.numer * denom
	
	def max(that: Rational2) = if (this.less(that)) that else this
	
	def add(that:Rational2) =
		new Rational2(
			numer * that.denom + that.numer * denom,
			denom * that.denom)

	def neg: Rational2 = new Rational2(-numer, denom)
	
	def sub(that: Rational2) = add(that.neg)
	
	
	override def toString = {
		val g = gcd(numer, denom)
		numer/g + "/" + denom/g
	}
}