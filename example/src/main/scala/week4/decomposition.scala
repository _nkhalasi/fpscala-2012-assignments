package week4

trait Expr {
  //classification methods
  def isNumber: Boolean
  def isSum: Boolean
//  def isVar: Boolean
//  def isProd: Boolean
  
  //accessor methods
  def numValue: Int
  def leftOp: Expr
  def rightOp: Expr
//  def nameValue: String //required for Var
}

class Number(n: Int) extends Expr {
  def isNumber = true
  def isSum = false
  def numValue = n
  def leftOp = throw new Error("Number.leftOp")
  def rightOp = throw new Error("Number.rightOp")
}

class Sum(e1: Expr, e2: Expr) extends Expr {
  def isNumber = false
  def isSum = true
  def numValue: Int = throw new Error("Sum.numValue")
  def leftOp: Expr = e1
  def rightOp: Expr = e2
}

//class Prod(e1: Expr, e2: Expr) extends Expr {
  
//}

//class Var(x: String) extends Expr {
  
//}

object test {
  def eval(e: Expr): Int = {
    if (e.isNumber) e.numValue
    else if (e.isSum) eval(e.leftOp) + eval(e.rightOp)
    else throw new Error("Unknown expression " + e)
  }
  
  assert(eval(new Sum(new Number(1), new Number(2))) == 3)
}