package week6

object maps {
  val romanNumerals = Map('I' -> 1, 'V' -> 5, 'X' -> 10)
                                                  //> romanNumerals  : scala.collection.immutable.Map[Char,Int] = Map(I -> 1, V -> 
                                                  //| 5, X -> 10)
  
  val capitalOfCountry = Map("US" -> "Washington", "Switzerland" -> "Bern")
                                                  //> capitalOfCountry  : scala.collection.immutable.Map[java.lang.String,java.lan
                                                  //| g.String] = Map(US -> Washington, Switzerland -> Bern)
  
  
  capitalOfCountry("US")                          //> res0: java.lang.String = Washington
  //capitalOfCountry("andorra")
  capitalOfCountry get "andorra"                  //> res1: Option[java.lang.String] = None
  capitalOfCountry get "US"                       //> res2: Option[java.lang.String] = Some(Washington)
 
  def showCapital(country: String) = capitalOfCountry get country match {
    case Some(capital) => capital
    case None => "missing data"
  }                                               //> showCapital: (country: String)java.lang.String
  
  showCapital("US")                               //> res3: java.lang.String = Washington
  showCapital("Andorra")                          //> res4: java.lang.String = missing data
  
  val cap1 = capitalOfCountry withDefaultValue "<unknown>"
                                                  //> cap1  : scala.collection.immutable.Map[java.lang.String,java.lang.String] = 
                                                  //| Map(US -> Washington, Switzerland -> Bern)
  cap1("Andorra")                                 //> res5: java.lang.String = <unknown>
}