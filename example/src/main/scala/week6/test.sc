package week6

object test {
  
  val xs = Array(1,2,3,4)                         //> xs  : Array[Int] = Array(1, 2, 3, 4)
  val ys = Array(5,10,15,20)                      //> ys  : Array[Int] = Array(5, 10, 15, 20)
  xs map (x => x * 2)                             //> res0: Array[Int] = Array(2, 4, 6, 8)
  
  val s = "Hello World"                           //> s  : java.lang.String = Hello World
  s filter (c => c.isUpper)                       //> res1: String = HW
  s exists (c => c.isUpper)                       //> res2: Boolean = true
  s forall (c => c.isUpper)                       //> res3: Boolean = false
  
  val pairs = List(1,2,3) zip s                   //> pairs  : List[(Int, Char)] = List((1,H), (2,e), (3,l))
  pairs unzip                                     //> res4: (List[Int], List[Char]) = (List(1, 2, 3),List(H, e, l))
  
  s flatMap (c => List('.', c))                   //> res5: String = .H.e.l.l.o. .W.o.r.l.d
  
  xs.sum                                          //> res6: Int = 10
  xs.max                                          //> res7: Int = 4
  
  //scalar product - version 1
  (xs zip ys) map (xy => xy._1 * xy._2) sum       //> res8: Int = 150

  //scalar product - version 2
  ((xs zip ys) map{ case (x, y) => x * y }) sum   //> res9: Int = 150
  
  def isPrime(n: Int): Boolean = (2 until n) forall (d => n % d != 0)
                                                  //> isPrime: (n: Int)Boolean
  isPrime(4)                                      //> res10: Boolean = false
  isPrime(7)                                      //> res11: Boolean = true
}