package week7

object test {
  val problem = new Pouring(Vector(4, 9, 19))     //> problem  : week7.Pouring = week7.Pouring@46b8c8e6
  
  
  //problem.moves
  //problem.pathSets
  //problem.pathSets.take(3).toList
  problem.solution(17)                            //> res0: Stream[week7.test.problem.Path] = Stream(Fill(0) Pour(0,2) Fill(0) Pou
                                                  //| r(0,2) Fill(1) Pour(1,2)--> Vector(0, 0, 17), ?)
}