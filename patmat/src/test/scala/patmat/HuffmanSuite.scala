package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
  trait TestTrees {
    val t0 = Leaf('a', 1)
    val t1 = Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5)
    val t2 = Fork(Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5), Leaf('d',4), List('a','b','d'), 9)
  }
  
  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t0) === 1)
      assert(weight(t1) === 5)
      assert(weight(t2) === 9)
    }
  }

  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t0) === List('a'))
      assert(chars(t1) === List('a', 'b'))
      assert(chars(t2) === List('a','b','d'))
    }
  }
  
  test("make CodeTree") {
    val t0 = makeCodeTree(makeCodeTree(makeCodeTree(Leaf('a', 4), Leaf('b', 2)), makeCodeTree(Leaf('c', 1), Leaf('d', 2))), Leaf('g', 5))
    assert(chars(t0) === List('a', 'b', 'c', 'd', 'g'))
    assert(weight(t0) === 14)
  }

  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }
  
  test("times(\"\") or times of nil") {
	assert(times(string2Chars("")) === List())
  }
  
  test("times(\"a\")") {
    assert(times(string2Chars("a")).sorted === List(('a', 1)))
  }
  
  test("times(\" hello, world \")") {
    assert(times(string2Chars(" hello, world ")).sorted === List((' ',3), (',', 1), ('d', 1), ('e', 1), ('h', 1), ('l', 3), ('o', 2), ('r', 1), ('w', 1)))
  }

  test("makeOrderedLeafList for empty list of char frequency") {
    assert(makeOrderedLeafList(List()) === List())
  }

  test("makeOrderedLeafList for list containing only one char frequency") {
    assert(makeOrderedLeafList(List(('a', 10))) === List(Leaf('a', 10)))
  }

  test("makeOrderedLeafList for \" hello, world \"") {
    val freqs = times(string2Chars(" hello, world "))
    assert(makeOrderedLeafList(freqs).sortBy(_.char) === List(Leaf(' ', 3), Leaf(',', 1), Leaf('d', 1), Leaf('e', 1), 
                                                              Leaf('h', 1), Leaf('l', 3), Leaf('o', 2), Leaf('r', 1), 
				    										  Leaf('w', 1)))
  }

  test("makeOrderedLeafList for some frequency table - 1") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e',1), Leaf('t',2), Leaf('x',3)))
  }
  
  test("makeOrderedLeafList for some frequency table - 2") {
    assert(makeOrderedLeafList(List(('a', 2), ('e', 1), ('x', 3))) === List(Leaf('e',1), Leaf('a',2), Leaf('x',3)))
  }
  
  test("singleton() on empty list") {
    assert(singleton(List()) === false)
  }
  
  test("singleton(List(makeCodeTree(Leaf('a', 1), Leaf('b', 1)))) is true") {
    assert(singleton(List(makeCodeTree(Leaf('a', 1), Leaf('b', 1)))))
  }

  test("singleton(List(Leaf('a', 1), Leaf('b', 1))) is false") {
    assert(singleton(List(Leaf('a', 1), Leaf('b', 1))) === false)
  }

  test("combine of a leaf list with zero elements") {
    val leaflist = List()
    assert(combine(leaflist) === List())
  }

  test("combine of a leaf list with one element") {
    val leaflist = List(Leaf('e', 1))
    assert(combine(leaflist) === List(Leaf('e',1)))
  }

  test("combine of a leaf list with two elements") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2))
    assert(combine(leaflist) === List(Fork(Leaf('e',1), Leaf('t', 2), List('e', 't'), 3)))
  }

  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4)))
  }

  test("until on a leaf list with zero elements") {
    val leaflist = List()
    assert(until(singleton, combine)(leaflist) === List())
  }
  
  test("until on a leaf list with one element") {
    val leaflist = List(Leaf('e', 2))
    assert(until(singleton, combine)(leaflist) === List(Leaf('e', 2)))
  }
  
  test("until on a leaf list with two elements") {
    val leaflist = List(Leaf('t', 1), Leaf('e', 3))
    assert(until(singleton, combine)(leaflist) === List(Fork(Leaf('t', 1), Leaf('e', 3), List('t', 'e'), 4)))
  }
  
  test("until on some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(until(singleton, combine)(leaflist) ===
      	List(Fork(Fork(Leaf('e', 1), 
      				   Leaf('t', 2), 
      				   List('e', 't'), 
      				   3), 
      			   Leaf('x', 4), 
      			   List('e', 't', 'x'), 
      			   7))
      )
  }

  test("createCodeTree(\"go go gophers\") is optimal") {
    val a = string2Chars("go go gophers")
    val b = createCodeTree(a)
    val encodedBits = encode(b)(a)
//    println("Size of encoded bits " + encodedBits.size)
//    println("Encoded "  + encodedBits)
    println("Code Tree " + b)
    assert(encodedBits.size === 37)
  }
  
  test("createCodeTree for \" hello, world \"") {
    assert(createCodeTree(string2Chars(" hello, world ")) === 
			Fork(
			    Fork(
			        Leaf('l',3),
			        Leaf(' ',3),
			        List('l', ' '),
			        6),
			    Fork(
			        Fork(
			            Leaf('o',2),
			            Fork(
			                Leaf('h',1),
			                Leaf('e',1),
			                List('h', 'e'),
			                2),
			            List('o', 'h', 'e'),
			            4),
			        Fork(
			            Fork(
			                Leaf(',',1),
			                Leaf('w',1),
			                List(',', 'w'),
			                2),
			            Fork(
			                Leaf('r',1),
			                Leaf('d',1),
			                List('r', 'd'),
			                2),
			            List(',', 'w', 'r', 'd'),
			            4),
			        List('o', 'h', 'e', ',', 'w', 'r', 'd'),
			        8),
			    List('l', ' ', 'o', 'h', 'e', ',', 'w', 'r', 'd'),
			    14)
//    		Fork(Fork(Fork(Leaf('h', 1),
//    					   Leaf('e', 1),
//    					   List('h', 'e'),
//    					   2), 
//    				  Leaf(' ', 3),
//    				  List('h', 'e', ' '),
//    				  5), 
//    		     Fork(Fork(Fork(Leaf('r', 1),
//    		                    Leaf('d', 1),
//    		                    List('r', 'd'),
//    		                    2),
//    		               Fork(Leaf(',', 1),
//    		                    Leaf('w', 1),
//    		                    List(',', 'w'),
//    		                    2),
//    		               List('r', 'd', ',', 'w'),
//    		               4),
//    		          Fork(Leaf('o', 2),
//    		               Leaf('l', 3),
//    		               List('o', 'l'),
//    		               5),
//    		          List('r', 'd', ',', 'w', 'o', 'l'),
//    		          9), 
//    		     List('h', 'e', ' ', 'r', 'd', ',', 'w', 'o', 'l'), 
//    		     14)
        )
  }

  test("encode simple text ab") {
    new TestTrees {
      assert(encode(t1)("ab".toList) === List(0,1))
    }
  }

  test("encode string hello") {
    val hello = string2Chars("hello")
    val helloCodeTree = createCodeTree(hello)
    val encoded = encode(helloCodeTree)(hello)
    val decoded = decode(helloCodeTree, encoded)
    assert(decoded == hello)
  }
  
  test("encode and decode \" hello, world \" ") {
    val helloWorldChars = string2Chars(" hello, world ")
    val helloWorldCodeTree = createCodeTree(helloWorldChars)
    val encoded = encode(helloWorldCodeTree)(helloWorldChars)
    assert(decode(helloWorldCodeTree, encoded) === helloWorldChars)
  }
  
  test("decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }
  
  test("decoded french code") {
    assert(decodedSecret === "huffmanestcool".toList)
  }
  
  test("quickEncode on hello") {
    val hello = string2Chars("hello")
    val helloCodeTree = createCodeTree(hello)
    val encoded = quickEncode(helloCodeTree)(hello)
    assert(encoded === List(0,0,0,1,1,1,1,1,1,0))
  }
}
