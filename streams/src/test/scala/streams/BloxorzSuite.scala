package streams

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import Bloxorz._

@RunWith(classOf[JUnitRunner])
class BloxorzSuite extends FunSuite {

  trait SolutionChecker extends GameDef with Solver with StringParserTerrain {
    /**
     * This method applies a list of moves `ls` to the block at position
     * `startPos`. This can be used to verify if a certain list of moves
     * is a valid solution, i.e. leads to the goal.
     */
    def solve(ls: List[Move]): Block =
      ls.foldLeft(startBlock) {
        case (block, move) => move match {
          case Left => block.left
          case Right => block.right
          case Up => block.up
          case Down => block.down
        }
      }
  }

  trait Level01 extends SolutionChecker {
    val level =
      """So
        |--""".stripMargin
  }

  trait Level02 extends SolutionChecker {
    val level =
      """-o
        |-S""".stripMargin
  }

  trait Level1 extends SolutionChecker {
    val level =
      """Soo---
        |oooT--""".stripMargin
  }

  trait Level2 extends SolutionChecker {
    /* terrain for level 2*/

    val level =
      """ooo-------
      |oSoooo----
      |ooooooooo-
      |-ooooooooo
      |-----ooToo
      |------ooo-""".stripMargin

    val optsolution = List(Right, Right, Down, Right, Right, Right, Down)
  }

  test("terrain function level 2") {
    new Level2 {
      assert(terrain(Pos(0, 0)), "0,0")
      assert(terrain(Pos(1, 1)), "1,1 Start")
      assert(!terrain(Pos(4, 11)), "4,11")
      assert(!terrain(Pos(4, 3)), "4,3")
    }
  }

  test("findChar level 1") {
    new Level1 {
      assert(startPos === Pos(0, 0))
      assert(goal === Pos(1, 3))
    }
  }

  test("findChar level 2") {
    new Level2 {
      assert(startPos === Pos(1, 1))
      assert(goal === Pos(4, 7))
    }
  }

  test("neighborsWithHistory level 01") {
    new Level01 {
      val startingBlock = Block(Pos(0, 0), Pos(0, 0))
      assert(neighborsWithHistory(startingBlock, Nil).toSet === Set())
    }
  }

  test("neighborsWithHistory level 02") {
    new Level02 {
      val startingBlock = Block(Pos(1, 1), Pos(1, 1))
      assert(neighborsWithHistory(startingBlock, Nil).toSet === Set())
    }
  }

  test("neighborsWithHistory level 1") {
    new Level1 {
      val startingBlock = Block(Pos(0, 0), Pos(0, 0))
      assert(neighborsWithHistory(startingBlock, Nil).toSet === Set((Block(Pos(0, 1), Pos(0, 2)), List(Right))))
    }
  }

  test("neighborsWithHistory level 2") {
    new Level2 {
      assert(neighborsWithHistory(Block(Pos(1, 1), Pos(1, 1)), List(Left, Up)).toSet ===
        Set(
          (Block(Pos(1, 2), Pos(1, 3)), List(Right, Left, Up)),
          (Block(Pos(2, 1), Pos(3, 1)), List(Down, Left, Up))))
    }
  }

  test("neighborsOnly level 2") {
    new Level2 {
      assert(newNeighborsOnly(
        Set(
          (Block(Pos(1, 2), Pos(1, 3)), List(Right, Left, Up)),
          (Block(Pos(2, 1), Pos(3, 1)), List(Down, Left, Up))).toStream,

        Set(Block(Pos(1, 2), Pos(1, 3)), Block(Pos(1, 1), Pos(1, 1))))
        ===
        Set(
          (Block(Pos(2, 1), Pos(3, 1)), List(Down, Left, Up))).toStream)
    }
  }

  test("optimal solution for level 2") {
    new Level2 {
      assert(solve(solution) === Block(goal, goal))
    }
  }

  test("optimal solution length for level 2") {
    new Level2 {
      assert(solution.length === optsolution.length)
    }
  }
}
