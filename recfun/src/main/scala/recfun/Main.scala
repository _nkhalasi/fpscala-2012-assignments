package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  class InvalidIndexException(msg: String) extends Exception(msg)

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if (c < 0 || c > r) throw new InvalidIndexException("Invalid row and/or column value")
    if (c == 0 || c == r) 1
    else (pascal(c - 1, r - 1) + pascal(c, r - 1))
  }

  /**
   * Exercise 2
   */
  class OutOfSequenceException(msg:String) extends Exception(msg)
  
  import scala.collection.immutable.Stack
  def balance(chars: List[Char]): Boolean = {
    def checkForBalancedParentheses(currentChar: Char, openingParentheses: Stack[Char]): Stack[Char] = {
      currentChar match {
        case '(' => 
          openingParentheses push currentChar
        case ')' =>
          openingParentheses.length match {
            case 0 => //out of sequence
              throw new OutOfSequenceException(currentChar + " is out of sequence")
            case _ =>
              openingParentheses.pop
          }
        case _ => //non-parentheses character...skip
          openingParentheses
      }
    }

    def balanceIter(currentChar: Char,
      allOtherChars: List[Char],
      openingParentheses: Stack[Char] = new Stack[Char]): Boolean = {
      if (!allOtherChars.isEmpty) {
        try {
          val updatedStack = checkForBalancedParentheses(currentChar, openingParentheses)
          balanceIter(allOtherChars.head, allOtherChars.tail, updatedStack)
        } catch {
          case _:OutOfSequenceException => false
        }
      } else {
        try {
          val updatedStack = checkForBalancedParentheses(currentChar, openingParentheses)
          updatedStack.length == 0
        } catch {
          case _:OutOfSequenceException => false
        }
      }
    }

    balanceIter(chars.head, chars.tail)
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    val sortedCoins = coins.sorted
    if (money == 0) 1
    else if (money < 0) 0
    else if (sortedCoins.length == 0 ) 0
    else {
      countChange(money-coins.head, coins) + countChange(money, coins.tail)
    }
  }
}
