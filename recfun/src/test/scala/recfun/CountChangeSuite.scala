package recfun

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CountChangeSuite extends FunSuite {
  import Main.countChange
  test("example from instructions") {
    assert(countChange(4,List(1,2)) === 3)
  }
  
  test("5 cents from 1,2 and 5 cents") {
    assert(countChange(5, List(1,2,5)) === 4)
  }

  test("sorted CHF") {
    assert(countChange(300,List(5,10,20,50,100,200,500)) === 1022)
  }

  test("no pennies") {
    assert(countChange(301,List(5,10,20,50,100,200,500)) === 0)
  }

  test("unsorted CHF") {
    assert(countChange(300,List(500,5,50,100,20,200,10)) === 1022)
  }
  
  test("no denominations") {
    assert(countChange(200, List()) === 0)
  }
  
  test("zero amount") {
    assert(countChange(0, List(1,2,3)) === 1)
  }
  
  test("negative amount") {
    assert(countChange(-5, List(1,2)) === 0)
  }
}
