package recfun

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PascalSuite extends FunSuite {
  import Main.pascal
  import Main.InvalidIndexException
  test("col=0, row=0") {
    assert(pascal(0,0) == 1)
  }
  test("col=0,row=2") {
    assert(pascal(0,2) === 1)
  }
  test("col=1,row=2") {
    assert(pascal(1,2) === 2)
  }
  test("col=1,row=3") {
    assert(pascal(1,3) === 3)
  }
  test("col=6, row=8") {
    assert(pascal(6,8) == 28)
  }
  test("col=-1, row=1") {
    intercept[InvalidIndexException] {
      assert(pascal(-1, 1) === 1)
    }
  }
  test("col=5, row=4") {
    intercept[InvalidIndexException] {
      assert(pascal(5,4) === 1)
    }
  }
  test("col=7, row=14") {
    assert(pascal(7,14) == 3432)
  }
}
