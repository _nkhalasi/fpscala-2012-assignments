package forcomp

import common._

object AnagramsCopy {

  /** A word is simply a `String`. */
  type Word = String

  /** A sentence is a `List` of words. */
  type Sentence = List[Word]

  /**
   * `Occurrences` is a `List` of pairs of characters and positive integers saying
   *  how often the character appears.
   *  This list is sorted alphabetically w.r.t. to the character in each pair.
   *  All characters in the occurrence list are lowercase.
   *
   *  Any list of pairs of lowercase characters and their frequency which is not sorted
   *  is **not** an occurrence list.
   *
   *  Note: If the frequency of some character is zero, then that character should not be
   *  in the list.
   */
  type Occurrences = List[(Char, Int)]

  /**
   * The dictionary is simply a sequence of words.
   *  It is predefined and obtained as a sequence using the utility method `loadDictionary`.
   */
  val dictionary: List[Word] = loadDictionary

  /**
   * Converts the word into its character occurence list.
   *
   *  Note: the uppercase and lowercase version of the character are treated as the
   *  same character, and are represented as a lowercase character in the occurrence list.
   */
  def wordOccurrences(w: Word): Occurrences =
    w.toLowerCase.groupBy(chr => chr).map { case (chr, lst) => (chr, lst.size) }.toList.sorted

  /** Converts a sentence into its character occurrence list. */
  def sentenceOccurrences(s: Sentence): Occurrences =
    wordOccurrences(s mkString)

  /**
   * The `dictionaryByOccurrences` is a `Map` from different occurrences to a sequence of all
   *  the words that have that occurrence count.
   *  This map serves as an easy way to obtain all the anagrams of a word given its occurrence list.
   *
   *  For example, the word "eat" has the following character occurrence list:
   *
   *     `List(('a', 1), ('e', 1), ('t', 1))`
   *
   *  Incidentally, so do the words "ate" and "tea".
   *
   *  This means that the `dictionaryByOccurrences` map will contain an entry:
   *
   *    List(('a', 1), ('e', 1), ('t', 1)) -> Seq("ate", "eat", "tea")
   *
   */
  lazy val dictionaryByOccurrences: Map[Occurrences, List[Word]] =
    dictionary groupBy wordOccurrences withDefaultValue List[Word]()

  /** Returns all the anagrams of a given word. */
  def wordAnagrams(word: Word): List[Word] =
    dictionaryByOccurrences(wordOccurrences(word))

  /**
   * Returns the list of all subsets of the occurrence list.
   *  This includes the occurrence itself, i.e. `List(('k', 1), ('o', 1))`
   *  is a subset of `List(('k', 1), ('o', 1))`.
   *  It also include the empty subset `List()`.
   *
   *  Example: the subsets of the occurrence list `List(('a', 2), ('b', 2))` are:
   *
   *    List(
   *      List(),
   *      List(('a', 1)),
   *      List(('a', 2)),
   *      List(('b', 1)),
   *      List(('a', 1), ('b', 1)),
   *      List(('a', 2), ('b', 1)),
   *      List(('b', 2)),
   *      List(('a', 1), ('b', 2)),
   *      List(('a', 2), ('b', 2))
   *    )
   *
   *  Note that the order of the occurrence list subsets does not matter -- the subsets
   *  in the example above could have been displayed in some other order.
   */
  def combinations(occurrences: Occurrences): List[Occurrences] = {
    def expanded(xs: (Char, Int)): List[Occurrences] =
      (for (xi <- 1 to xs._2) yield (xs._1, xi) :: Nil) toList

    def helper(occurrences: Occurrences): List[Occurrences] = {
      occurrences match {
        case Nil => Nil
        case xs :: Nil =>
          expanded(xs)
        case xs :: ys =>
          val zs = expanded(xs)
          val others = helper(ys)
          val paired = for {
            occurrence <- others
            x <- zs
          } yield x ++ occurrence
          zs ++ others ++ paired
      }
    }

    List() :: helper(occurrences)
  }

  def combinations_5(occurrences: Occurrences): List[Occurrences] = {
    def helper(occur: Occurrences): List[Occurrences] = {
      println("Entered with occurrences = " + occur)
      if (occur.isEmpty) List(List())
      else {
        val res =
          for {
            x <- helper(occur.tail)
            y <- 1 to occur.head._2
          } yield (occur.head._1, y) :: x
        println("Results for occurrences = " + occur + " are " + res)
        res
      }
    }
    println()
    helper(occurrences)
  }

  //submitted solution, partially correct :-(
  def combinations_4(occurrences: Occurrences): List[Occurrences] = {

    def combinationsHelper(occur: Occurrences): List[Occurrences] = {
      if (occur.isEmpty) List(List())
      else {
        val combinationsForFirst = (for (i <- 1 to occur.head._2) yield List((occur.head._1, i))) toList
        val combinationsForLast = (for (i <- 1 to occur.last._2) yield List((occur.last._1, i))) toList
        val combinationsForOthers1 = for {
          a <- combinationsHelper(occur.tail)
          b <- 1 to occur.head._2
        } yield (occur.head._1, b) :: a
        List(List()) ::: combinationsForFirst ::: combinationsForOthers1 ::: combinationsForLast
      }
    }

    combinationsHelper(occurrences)
  }

  def combinations_3(occurrences: Occurrences): List[Occurrences] = {
    /*
     * The for-expr is getting all the combos containing the current occurrences head which is half of the equation.
     * Now we need to figure out how to aggregate this result to the list of combinations that do not contain the
     * head (which in turn will satisfy the empty element case as well singleton cases).
     */
    def helper(occur: Occurrences): List[Occurrences] = {
      if (occur.isEmpty) List(List())
      else {
        for {
          x <- helper(occur.tail)
          y <- 1 to occur.head._2
        } yield (occur.head._1, y) :: x
      }
    }
    println()
    val initial = List(List())
    if (occurrences.isEmpty) initial
    else {
      //      val res = initial ::: helper(occurrences.init) ::: helper(occurrences.tail) ::: helper(List(occurrences.head, occurrences.last)) ::: helper(occurrences)
      val res = initial ::: helper(occurrences.init) ::: helper(occurrences.tail) ::: helper(List(occurrences.head, occurrences.last)) ::: helper(occurrences)
      println("Result for occur=" + occurrences + " is " + res)
      res
    }
  }

  def combinations_2(occurrences: Occurrences): List[Occurrences] = {
    def helper(occur: Occurrences, accum: List[Occurrences]): List[Occurrences] = {
      occur match {
        case (x :: Nil) => ((1 to x._2) map (i => List((x._1, i))) toList) ::: accum
        case (x :: xs) => {
          (for {
            x <- helper(occur.tail, accum)
            y <- 1 to occur.head._2
          } yield (occur.head._1, y) :: x) ::: accum
        }
        case Nil => List(List()) ::: accum
      }
    }
    println()
    helper(occurrences, List())
  }

  def combinations_1(occurrences: Occurrences): List[Occurrences] = {
    def relatedOccurrences(occur: (Char, Int)): List[Occurrences] = {
      (1 to occur._2) map (i => List((occur._1, i))) toList
    }

    def helper(occur: Occurrences, start: Int): List[Occurrences] = {
      println("Occurrence: " + occur + ", Start: " + start)
      if (occur.isEmpty)
        List(List())
      else {
        println("Head: " + occur.head + ", Tail: " + occur.tail)
        val a =
          for {
            x <- helper(occur.tail, start + 1)
            y <- 1 to occur.head._2
          } yield (occur.head._1, y) :: x
        println("Result: " + a)
        a
      }
    }
    println()
    val a = occurrences flatMap relatedOccurrences
    println("Related Occurrences: " + a)
    val b = helper(occurrences, 1)
    println("Others: " + b)
    a ::: b
  }

  /**
   * Subtracts occurrence list `y` from occurrence list `x`.
   *
   *  The precondition is that the occurrence list `y` is a subset of
   *  the occurrence list `x` -- any character appearing in `y` must
   *  appear in `x`, and its frequency in `y` must be smaller or equal
   *  than its frequency in `x`.
   *
   *  Note: the resulting value is an occurrence - meaning it is sorted
   *  and has no zero-entries.
   */
  def subtract(x: Occurrences, y: Occurrences): Occurrences = {

    def canInclude(xi: (Char, Int)): Boolean = !(y contains xi)
    def findXi(xi: (Char, Int)): (Char, Int) = {
      y.find(_._1 == xi._1) match {
        case Some(yi) => yi
        case None => (xi._1, 0)
      }
    }

    for {
      xi <- x
      if (canInclude(xi))
    } yield (xi._1, (xi._2 - findXi(xi)._2))
  }

  /**
   * Returns a list of all anagram sentences of the given sentence.
   *
   *  An anagram of a sentence is formed by taking the occurrences of all the characters of
   *  all the words in the sentence, and producing all possible combinations of words with those characters,
   *  such that the words have to be from the dictionary.
   *
   *  The number of words in the sentence and its anagrams does not have to correspond.
   *  For example, the sentence `List("I", "love", "you")` is an anagram of the sentence `List("You", "olive")`.
   *
   *  Also, two sentences with the same words but in a different order are considered two different anagrams.
   *  For example, sentences `List("You", "olive")` and `List("olive", "you")` are different anagrams of
   *  `List("I", "love", "you")`.
   *
   *  Here is a full example of a sentence `List("Yes", "man")` and its anagrams for our dictionary:
   *
   *    List(
   *      List(en, as, my),
   *      List(en, my, as),
   *      List(man, yes),
   *      List(men, say),
   *      List(as, en, my),
   *      List(as, my, en),
   *      List(sane, my),
   *      List(Sean, my),
   *      List(my, en, as),
   *      List(my, as, en),
   *      List(my, sane),
   *      List(my, Sean),
   *      List(say, men),
   *      List(yes, man)
   *    )
   *
   *  The different sentences do not have to be output in the order shown above - any order is fine as long as
   *  all the anagrams are there. Every returned word has to exist in the dictionary.
   *
   *  Note: in case that the words of the sentence are in the dictionary, then the sentence is the anagram of itself,
   *  so it has to be returned in this list.
   *
   *  Note: There is only one anagram of an empty sentence.
   */
  def sentenceAnagrams(sentence: Sentence): List[Sentence] = {
    val occurrences = sentenceOccurrences(sentence)
    println("Sentence Occurrences = " + occurrences)
    val combinationList = combinations(occurrences)
    println("Combinations = " + combinationList)
    List(sentence)
  }
  
  def sentenceAnagrams_1(sentence: Sentence): List[Sentence] = {
    def possibleAnagrams(chars: String): List[Sentence] = {
      //      println("Sentence: " + chars)
      //      println("Length: " + chars.length)
      val b =
        for {
          split <- 1 to chars.length
          anagrams <- wordAnagrams(chars take split)
          others <- possibleAnagrams(chars drop split)
        } yield anagrams :: others
      //      println("Result is " + b)
      b.toList
    }

    if (sentence.isEmpty) List(List())
    else possibleAnagrams(sentence mkString)
  }

}
