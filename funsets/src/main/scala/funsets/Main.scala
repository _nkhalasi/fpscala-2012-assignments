package funsets

object Main extends App {
  import FunSets._
  println(contains(singletonSet(1), 1))
  
  val s:Set = x => x > 0 && x < 5
  val ss = map(s, x => x * x)
  printSet(s)
  println()
  printSet(ss)
}
