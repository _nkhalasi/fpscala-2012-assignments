package funsets

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 *  - run the "test" command in the SBT console
 *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {
  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }

  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   *
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   *
   *   val s1 = singletonSet(1)
   *
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   *
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   *
   */

  trait TestSetsUsingSingletonSet {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
    val s4 = singletonSet(4)
  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   *
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSet(1) contains 1") {
    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3".
     */
    new TestSetsUsingSingletonSet {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "singletonSet(1) should contain 1")
      assert(!contains(s1, 2), "singletonSet(1) should not contain 2")
    }
  }
  test("singletonSet(2) contains 2") {
    new TestSetsUsingSingletonSet {
      assert(contains(s2, 2), "singletonSet(2) should contain 2")
      assert(!contains(s2, 1), "singletonSet(2) should not contain 1")
    }
  }
  test("singletonSet(3) contains 3") {
    new TestSetsUsingSingletonSet {
      assert(contains(s3, 3), "singletonSet(3) should contain 3")
      assert(!contains(s3, 1), "singletonSet(3) should not contain 1")
    }
  }
  test("singletonSet(-1) contains -1") {
    new TestSetsUsingSingletonSet {
      val sMinus1 = singletonSet(-1)
      assert(contains(sMinus1, -1), "singletonSet(-1) should contain -1")
      assert(!contains(sMinus1, 1), "singletonSet(-1) should not contain 1")
    }
  }

  test("union contains all elements") {
    new TestSetsUsingSingletonSet {
      val s = union(s1, s2)
      assert(contains(s, 1), "Union 1")
      assert(contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
    }
  }

  test("intersect contains only common elements") {
    new TestSetsUsingSingletonSet {
      val s = intersect(union(s1, s2), union(s2, s3))
      assert(contains(s, 2), "intersection should contain 2")
      assert(!contains(s, 1), "intersection should not contain 1")
      assert(!contains(s, 3), "intersection should not contain 3")
    }
  }

  test("diff returns elements of first set that do not exist in second set") {
    new TestSetsUsingSingletonSet {
      val s = diff(union(s1, s2), union(s2, s3))
      assert(contains(s, 1), "diff should contain 1")
      assert(!contains(s, 2), "intersection should not contain 2")
      assert(!contains(s, 3), "intersection should not contain 3")
    }
    new TestSetsUsingSingletonSet {
      val s = diff(union(union(s1, s2), s3), union(s2, s4))
      assert(contains(s, 1), "diff should contain 1")
      assert(contains(s, 3), "diff should contain 3")
      assert(!contains(s, 2), "intersection should not contain 2")
      assert(!contains(s, 4), "intersection should not contain 4")
    }
  }

  test("filter positive numbers from set") {
    new TestSetsUsingSingletonSet {
      val s = union(union(union(s1, s2), s3), s4)
      assert(!contains(filter(s, x => x % 2 == 0), 1), "filtered set should not contain 1")
      assert(contains(filter(s, x => x % 2 == 0), 2), "filtered set should contain 2")
      assert(!contains(filter(s, x => x % 2 == 0), 3), "filtered set should not contain 3")
      assert(contains(filter(s, x => x % 2 == 0), 4), "filtered set should contain 4")
    }

    assert(!contains(filter(first10NegativeNumbers, x => x % 3 == 0), -1), "filtered set should not contain -1")
    assert(contains(filter(first10NegativeNumbers, x => x % 3 == 0), -3), "filtered set should contain -3")
    assert(contains(filter(first10NegativeNumbers, x => x % 3 == 0), -6), "filtered set should contain -6")
    assert(contains(filter(first10NegativeNumbers, x => x % 3 == 0), -9), "filtered set should contain -9")
    assert(!contains(filter(first10NegativeNumbers, x => x % 3 == 0), -10), "filtered set should not contain -10")
  }

  test("+ve number and zero are not in first10NegativeNumber set") {
    assert(!contains(first10NegativeNumbers, 0), "Zero is not a negative number")
    assert(!contains(first10NegativeNumbers, 10), "10 is not a negative number")
  }
  test("Low -ve numbers will not appear in first10NegativeNumber set") {
    assert(!contains(first10NegativeNumbers, -11), "-11 is too low a negative number")
    assert(!contains(first10NegativeNumbers, -110), "-110 is too low a negative number")
  }
  test("first10NegativeNumbers set contains values between -1 and -10 both inclusive") {
    def assertContains(s: Set, x: Int) = assert(contains(s, x), x + " should appear in the set")
    (-10 to -1) foreach {
      assertContains(first10NegativeNumbers, _)
    }
  }
  
  test("test if all elements in the set are even numbers") {
    new TestSetsUsingSingletonSet {
	  val s = union(s2, s4)
	  assert(forall(s, x => x % 2 == 0), "Not all elements are even numbers")
	  assert(!forall(s, x => x % 2 != 0), "Not all elements are even numbers")
    }
  }
  test("test if all elements in the set are odd numbers") {
    new TestSetsUsingSingletonSet {
	  val s = union(s1, s3)
	  assert(forall(s, x => x % 2 != 0), "Not all elements are odd numbers")
	  assert(!forall(s, x => x % 2 == 0), "Not all elements are odd numbers")
    }
  }
  test("test if all elements in the set are -ve numbers") {
	  assert(forall(first10NegativeNumbers, x => x < 0), "One or more +ve number found")
	  assert(!forall(first10NegativeNumbers, x => x >= 0), "One or more +ve number found")
  }

  test("test if there is atleast one even number") {
    new TestSetsUsingSingletonSet {
	  val s = union(union(s1, s3), s2)
	  assert(exists(s, x => x % 2 == 0) === true, "There is no even number")
    }
  }
  test("test if there is atleast one odd number") {
    new TestSetsUsingSingletonSet {
	  val s = union(union(s2, s4), s3)
	  assert(exists(s, x => x % 2 != 0) === true, "There is no odd number")
    }
  }
  test("test if there is atleast one +ve number") {
    new TestSetsUsingSingletonSet {
	  val s = union(union(s2, s3), first10NegativeNumbers)
	  assert(exists(s, x => x > 0) === true, "There is no +ve number")
    }
  }
  test("test if there is atleast one number divisible by 3") {
    new TestSetsUsingSingletonSet {
	  val s = union(union(s4, s3), first10NegativeNumbers)
	  assert(exists(s, x => x % 3 == 0) === true, "There is no number divisible by 3")
    }
  }
  test("test if x exists in the set such that x+x == x*x") {
    val evens2to10:Set = x => x % 2 == 0 && x > 0 && x <= 10
    assert(exists(evens2to10, x => x+x == x*x), "2 not found in the set")
    val zeroAndOne = union(singletonSet(0), singletonSet(1))
    assert(exists(zeroAndOne, x => x+x == x*x), "0 not found in the set")
  }
  
  test("double each member of set") {
    val s:Set = x => x > 0 && x < 5
    val s1 = map(s, x => 2 * x)
    assert(!contains(s1,1))
    assert(contains(s1,2))
    assert(!contains(s1,3))
    assert(contains(s1,4))
    assert(contains(s1,6))
    assert(contains(s1,8))
  }
  test("increment each member of set") {
    val s:Set = x => x > 0 && x < 5
    val s1 = map(s, x => 1 + x)
    assert(!contains(s1,1))
    assert(contains(s1,2))
    assert(contains(s1,3))
    assert(contains(s1,4))
    assert(contains(s1,5))
    assert(!contains(s1,6))
  }
}
